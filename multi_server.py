#!/usr/bin/python

from http import server
import socket
from _thread import *
import sys
from bowling2 import Bowling2


host = "127.0.0.1"
port = 1234
connections = 0
users = []
gameStart = False

def start_server(host, port, connections):
    # declare a new socket
    ServerSocket = socket.socket()

    # try to bind the server to the socket on the port
    try:
        ServerSocket.bind((host, port))
    except socket.error as e:
        print(str(e)) # print binding error

    print(f'Server listening on port {port}...')

    # listen for connections
    ServerSocket.listen()

    while True:
        connections += 1
        accept_connections(ServerSocket, connections)

# accepts new connections
def accept_connections(ServerSocket, connections):
    Client, address = ServerSocket.accept()
    print(f'Now connected to {address[0]}:str({address[1]})')
    print("Number of connections: ", connections)
    if (connections < 5):
        start_new_thread(client_handler, (Client, ))
    elif (connections == 4):
        start(1)

def client_handler(connection):
    global started
    started = 0
    connection.send(str.encode("You are now connected to server...\n\n"))
    connection.send(str.encode("Please wait for up to 4 friends to connect. Press enter when ready.\n"))
    users.append(connection)
    while True:
        connection.recv(4)
        started = started + 1
        start(started)
    connection.close()

def start (started):
    if (started < 2):
        game = Bowling2(users)
        end = game.gameStart(users)
    if (end == 1):
        for x in range(len(users)):
            users[x].close()


start_server(host, port, connections)

