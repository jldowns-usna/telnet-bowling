import socket

def connect():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error as err:
        print ("Socket creation failed with error %s" %(err))

    port = 1234
    host = "127.0.0.1"
    try:
        s.connect((host, port))
        print(f"{colors.SUCCESS}Successfully connected to host.{colors.END}\n")
    except socket.error as err:
        print(f"{colors.ERROR}Error connecting to host. The server may not be running.{colors.END}")
        exit()