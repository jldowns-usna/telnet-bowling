# based on the bowing game in the BASIC computer games
from datetime import datetime
import random
import socket
from _thread import *
import sys
import os

class Bowling2:

    def __init__(self, users):
        self.pins = dict([('pin1', 'O'), ('pin2', 'O'),('pin3', 'O'),('pin4', 'O'),('pin5', 'O'),('pin6', 'O'),('pin7', 'O'),('pin8', 'O'),('pin9', 'O'),('pin10', 'O')])
        #self.connection = connection
        self.users = users
        random.seed(int(datetime.now().timestamp()))
        self.extraPoints = 0

    class colors:
        ERROR = '\033[91m'
        SUCCESS = '\033[92m'
        BLUE = '\033[94;5m'
        STRIKE = '\033[92;5m'
        SCORES = '\033[96;1;3m'
        GRAY = '\033[90m'
        END = '\033[0m'
        BALL = '\033[38;5;202m'

    def printHeader(self, connection):
        connection.send(str.encode(f"\033[1m\n\nWELCOME TO THE ALLEY\nBRING YOUR FRIENDS\n"))
        connection.send(str.encode(f"OKAY, LET'S FIRST GET ACQUAINTED\033[0m"))
        connection.send(str.encode("\n\nThe instructions:\n"))
        connection.send(str.encode("\nThe game of bowling takes true skill and finesse.\nYou can play with up to 4 players, and the computer"))
        connection.send(str.encode("\nwill keep score. You will be playing 10 frames. On the pin diagram,\nO shows a remaining pin"))
        connection.send(str.encode("\nAfter the game, the computer will show you your scores.\n"))

    def bowl1(self):
        frametotal = 0
        for x,y in self.pins.items():
            r = int(random.randint(0,100))
            if (r < 75):
                self.pins[x] = f'{self.colors.GRAY}+{self.colors.END}'
                frametotal = frametotal + 1
        return frametotal

    def bowl2(self):
        frametotal = 0
        for x,y in self.pins.items():
            r = int(random.randint(0, 100))
            if (self.pins[x] == f'{self.colors.GRAY}+{self.colors.END}'):
                pass
            else: 
                if (r > 20):
                    self.pins[x] = f'{self.colors.GRAY}+{self.colors.END}'
                    frametotal = frametotal + 1
        return frametotal

    def printPins(self, pins, rollNum, connection):
        #print header
        connection.send(str.encode(f"{self.colors.GRAY}------------------------------------------------------(\n" +
                                        f"------------------------------------------------------(\n{self.colors.END}"))
        i = 0
        for x, y in self.pins.items():
            if (i == 0):
                if(y == "O"):
                    connection.send(str.encode(y + "\n"))
                else:
                    connection.send(str.encode("\n"))
            elif (i == 1):
                if(y == "O"):
                    connection.send(str.encode("  " + y + "\n"))
                else:
                    connection.send(str.encode("   \n"))
            elif (i < 4):
                if (i == 2):
                    if(y == "O"):
                        connection.send(str.encode(y))
                    else:
                        connection.send(str.encode(" "))
                else:
                    if(y == "O"):
                        if(rollNum == 1):
                            connection.send(str.encode("   " + y + f"{self.colors.BALL}                  _{self.colors.END}\n"))
                        else:
                            connection.send(str.encode("   " + y + "\n"))
                    else:
                        if(rollNum == 1):
                            connection.send(str.encode("    " + f"{self.colors.BALL}                  _{self.colors.END}\n"))
                        else:
                            connection.send(str.encode("\n"))
            elif (i < 6):
                if(i == 4):
                    if(y == "O"):
                        connection.send(str.encode("  " + y))
                    else:
                        connection.send(str.encode("   "))
                else:
                    if(y == "O"):
                        if(rollNum == 1):
                            connection.send(str.encode("   " + y + f"{self.colors.BALL}               (_) {self.colors.END}= -\n"))
                        else:
                            connection.send(str.encode("   " + y + "\n"))
                    else:
                        if(rollNum == 1):
                            connection.send(str.encode("    " + f"{self.colors.BALL}               (_) {self.colors.END}= -\n"))
                        else:
                            connection.send(str.encode("\n"))
            elif (i < 8):
                if(i == 6):
                    if(y == "O"):
                        connection.send(str.encode(y))
                    else:
                        connection.send(str.encode(" "))
                else:
                    if(y == "O"):
                        connection.send(str.encode("   " + y + "\n"))
                    else:
                        connection.send(str.encode("    " + "\n"))
            elif (i < 9):
                if(y == "O"):
                    connection.send(str.encode("  " + y + "\n"))
                else:
                    connection.send(str.encode("   " + "\n"))
            elif(i == 9):
                if(y == "O"):
                    connection.send(str.encode(y + "\n"))
                else:
                    connection.send(str.encode(" \n"))
            elif(i == 10):
                break
            i = i+1
        #print footer
        connection.send(str.encode(f"{self.colors.GRAY}------------------------------------------------------(\n" +
                                        f"------------------------------------------------------(\n{self.colors.END}"))

    def resetPins(self):
        return dict([('pin1', 'O'), ('pin2', 'O'),('pin3', 'O'),('pin4', 'O'),('pin5', 'O'),('pin6', 'O'),('pin7', 'O'),('pin8', 'O'),('pin9', 'O'),('pin10', 'O')])

    def bowlFrame(self, frame_num, player, connection):
        self.pins = self.resetPins()
        display = True
        connection.send(str.encode(f"\n{self.colors.BLUE}HIT ENTER TO ROLL.{self.colors.END}\n"))
        #this looks really pretty I swear
        connection.send(str.encode(
           f" __  __  __  __\n" +
           f"{self.colors.ERROR} )({self.colors.END}__{self.colors.ERROR})({self.colors.END}__{self.colors.ERROR})({self.colors.END}__{self.colors.ERROR})({self.colors.END}\n" +
           f"/  {self.colors.ERROR})({self.colors.END}__{self.colors.ERROR})({self.colors.END}__{self.colors.ERROR})({self.colors.END}  \\\n" +
           f"| /  {self.colors.ERROR})({self.colors.END}__{self.colors.ERROR})({self.colors.END}  \ |\n" +
           f"| | /  {self.colors.ERROR})({self.colors.END}  \ | |\n" +
            "'-| | /  \ | |-'\n" +
            "  '-| |  | |-'\n" +
           f"    '-|  |-' {self.colors.BALL}.--.{self.colors.END}\n" +
           f"      '--'  {self.colors.BALL}/ .  \\\n" +
            "            \\'.  /\n" +
           f"             '--'{self.colors.END}\n"))
        connection.recv(4)
        score = self.bowl1()
        self.score_vals[player] += score
        if(self.extraPoints > 0):
            self.score_vals[player] += score
            self.extraPoints = self.extraPoints-1

        if (score == 10):
            connection.send(str.encode(f"{self.colors.STRIKE}" +
                "  ____ _____ ____  ___ _  _______\n" +
                "/ ___|_   _|  _ \|_ _| |/ / ____|\n" +
                "\___ \ | | | |_) || || ' /|  _|  \n" +
                " ___) || | |  _ < | || . \| |___ \n" +
                "|____/ |_| |_| \_\___|_|\_\_____|\n" +
                f"{self.colors.END}"))
            score = "X"
            self.extraPoints += 2
            display = False
           
            return score
        if (score == 0):
            connection.send(str.encode(f"{self.colors.ERROR}" +
            "   ____ _   _ _____ _____ _____ ____  \n" +
            "  / ___| | | |_   _|_   _| ____|  _ \ \n" +
            " | |  _| | | | | |   | | |  _| | |_) |\n" +
            " | |_| | |_| | | |   | | | |___|  _ < \n" +
            "  \____|\___/  |_|   |_| |_____|_| \_\\\n" +
            f"{self.colors.END}"))
            display = False
            score = "-"
        
        if(self.extraPoints > 0):
            score = score*2
            self.extraPoints = self.extraPoints-1

        self.printPins(self.pins, 1, connection)
        connection.send(str.encode(f"\n{self.colors.BLUE}HIT ENTER TO ROLL.{self.colors.END}\n"))
        connection.recv(4)

        score2 = self.bowl2()
        self.score_vals[player] += score2
        if(self.extraPoints > 0):
            self.score_vals[player] += score2
            self.extraPoints = self.extraPoints-1
            
        if (score2 == 0):
            connection.send(str.encode(f"{self.colors.ERROR}" +
            "   ____ _   _ _____ _____ _____ ____  \n" +
            "  / ___| | | |_   _|_   _| ____|  _ \ \n" +
            " | |  _| | | | | |   | | |  _| | |_) |\n" +
            " | |_| | |_| | | |   | | | |___|  _ < \n" +
            "  \____|\___/  |_|   |_| |_____|_| \_\\\n" +
            f"{self.colors.END}"))
            display = False
            score2 = "-"

        if(str(score).isdigit() and str(score2).isdigit()):
            score2 = score + score2
        else:
            score2 = str(score) + str(score2)
        if (score2 == 10):
            connection.send(str.encode(f"{self.colors.STRIKE}" +
            "  ____  ____   _    ____  _____ \n" +
            " / ___||  _ \ / \  |  _ \| ____|\n" +
            " \___ \| |_) / _ \ | |_) |  _|  \n" +
            "  ___) |  __/ ___ \|  _ <| |___ \n" +
            " |____/|_| /_/   \_\_| \_\_____|\n" +
            f"{self.colors.END}"))
            score2 = str(score) + "/"
            display = False
            self.extraPoints += 1

        if(display):
            self.printPins(self.pins, 2, connection)
        return score2
    
    def printScore(self, num_players, connection):
        connection.send(str.encode(f"\n\n{self.colors.SCORES}FINAL SCORES{self.colors.END}\n"))
        for x in range (num_players):
            connection.send(str.encode(f"\033[1mPLAYER " + str(x+1) + ":\033[0m " + str(self.score_vals[x]) + "\n"))

        connection.send(str.encode("\nTHANKS FOR PLAYING!\n"))
    
    def calcScore(self, player):
        total = 0
        for x in range(10):
            #get rid of backspace string literal
            self.scores[player][x] = self.scores[player][x].replace("\x08", "")
            #gutter
            if("-" in self.scores[player][x]):
                total += int(self.scores[player][x].replace("-", ""))
            #spare
            elif("/" in self.scores[player][x]):
                #total += int(self.scores[player][x].replace("/", ""))
                total += 10
            #strike
            elif("X" in self.scores[player][x]):
                total += 10
                extra = extra+2
            else:
                total += int(self.scores[player][x])
        
        return total
        
    
    def printScoreboard(self, player, frame_num, users):
        #print header
        for x in range(self.num_players):
            users[x].send(str.encode("\n\n\n+------+----+----+----+----+----+----+----+----+----+-----+-------+\n" +
                                            "| Name | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10  | Total |\n" +
                                            "+------+----+----+----+----+----+----+----+----+----+-----+-------+\n"))
            #print each player's score
            for i in range(self.num_players):
                users[x].send(str.encode("|" + str(i+1) + 
                                                "     |  " + self.scores[i][0] + "  |  " + self.scores[i][1] +
                                                "  |  " + self.scores[i][2] + "  |  " + self.scores[i][3] +
                                                "  |  " + self.scores[i][4] + "  |  " + self.scores[i][5] + 
                                                "  |  " + self.scores[i][6] + "  |  " + self.scores[i][7] + 
                                                "  |  " + self.scores[i][8] + "  |  " + self.scores[i][9] + 
                                                "   |" + str(self.score_vals[player]) + "     |\n" + 
                                                "+------+----+----+----+----+----+----+----+----+----+-----+-------+\n"))

    def gameStart(self, users):
        print('game start')
        self.num_players = len(users)
        self.scores = [["", "", "", "", "", "", "", "", "", ""], 
                       ["", "", "", "", "", "", "", "", "", ""],
                       ["", "", "", "", "", "", "", "", "", ""],
                       ["", "", "", "", "", "", "", "", "", ""]]
        self.score_vals = [0, 0, 0, 0]

        for x in range (self.num_players):
            self.printHeader(users[x])
        
        #10 frames
        for x in range(10):
            #each player
            for j in range(self.num_players):
                self.printScoreboard(j, x, users)
                value = str(self.bowlFrame(x, j, users[j]))
                if(len(value) == 2):
                    self.scores[j][x] = "\b" + "\b" + value
                else:
                    self.scores[j][x] = "\b" + value
        
        for x in range(self.num_players):
            total = self.calcScore(x)

        for x in range(self.num_players):
            users[x].send(str.encode(f"\n\n{self.colors.SCORES}FINAL SCORES{self.colors.END}\n"))
            self.printScore(self.num_players, users[x])
        
        return 1