IC322 Game

Anna-Grace Dumas & Matthew Lewis

Usage:
    In a terminal, run the server.py file to start the game server. You can use telnet to connect to 127.0.0.1 at port 1234. Up to four people may connect to the server to play the game. Each user only sees their bowl, but all users will recieve updated scores after each bowl.

Authors:
    Anna-Grace: Set up game framework, logic, and the skeleton for a socket-based server.
    Matthew: Added ASCII/terminal art
    Both: Worked together on creating a functional server-side game that connects to multiple clients.
